#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

static struct region create_region(void* addr, size_t size, bool extends){
  return (struct region ) {.addr = addr, .size = size, .extends = extends };
}

static struct region alloc_region  ( void const * addr, size_t query ) {
  if (!addr) {
    return REGION_INVALID;
  }
  void* addr_region = map_pages(addr, query, MAP_FIXED);
  block_size block_sz = {.bytes = region_actual_size( query )};
  block_init(addr_region, block_sz,  NULL);

  return create_region(addr_region, block_sz.bytes, true);  
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  if (block_splittable(block, query)) {
    struct block_header* addr_next = (struct block_header*) (block->contents + query); 
    block_size block_sz = {.bytes = block->capacity.bytes - query};
    block_init(addr_next, block_sz, block->next );
    block->capacity.bytes = query;
    block->next = addr_next; 
    return true;
  }
  return false;
}

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!block->next) return false;
  if (mergeable(block, block->next) ) {
    
    size_t cap_bytes = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes;
    block_capacity block_cap = {.bytes = cap_bytes};
    block->capacity = block_cap;
    block->next = block->next->next;
    return true;
  }
  return false;
}

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static struct block_search_result make_good_block(struct block_header* block) {
  return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block,};
}

static struct block_search_result make_corrupted_block(struct block_header* block) {
  return (struct block_search_result) {.type = BSR_CORRUPTED, .block = block,};
}

static struct block_search_result make_final_block(struct block_header* block) {
  return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block,};
}

static struct block_search_result find_good_or_last( struct block_header* restrict block, size_t sz )    {
  struct block_header* block_copy = block;
  struct block_header* block_last;
  if (!block_copy) {
    return make_corrupted_block(block_copy);
  }
  
  while (block_copy) {
    while(try_merge_with_next(block_copy));
    if (block_is_big_enough(sz, block_copy) && block_copy->is_free){
      return make_good_block(block_copy);
    }
    block_last = block_copy;
    block_copy = block_copy->next;
  }

  return make_final_block(block_last);
}

static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {

  struct block_search_result needed_block = find_good_or_last(block, query);
  if (needed_block.type == BSR_FOUND_GOOD_BLOCK ) {
    split_if_too_big(needed_block.block, query);
    needed_block.block->is_free = false;
  }
  return needed_block;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (!last) {
    return last;
  }
  struct region region = alloc_region(block_after(last), query);
  last->next = region.addr;
  if (try_merge_with_next(last)){
    return last;
  }
  return last->next;
}

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  struct block_search_result needed_block = try_memalloc_existing(query, heap_start);
  switch (needed_block.type)
  {
  case BSR_FOUND_GOOD_BLOCK: {
    split_if_too_big(needed_block.block, query);
    return needed_block.block;
  }

  case BSR_REACHED_END_NOT_FOUND: {
    size_t size_to_grow = query;
    if(needed_block.block->is_free) {
      size_to_grow =query - needed_block.block->capacity.bytes;
    }
    return try_memalloc_existing(query, grow_heap(needed_block.block, size_to_grow)).block;
  }

  default: {
    return NULL;
  }

  }
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
