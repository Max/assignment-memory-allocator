#include "mem.h"
#include "mem_internals.h"
#include "tests.h"


static void test_1() {
    printf("\nFIRST_TEST\n");
    void* addr = _malloc(10);
    debug_heap(stdout, HEAP_START);
    printf("\n");

    /*Соберем блок обратно в один, для того, чтобы использовать в последующих тестах*/
    _free(addr);

}

static void test_2() {
    printf("\nSECOND_TEST\n");
    void *block_1 = _malloc(14);
    void *block_2 = _malloc(25);
    debug_heap(stdout, HEAP_START);
    _free(block_1);
    debug_heap(stdout, HEAP_START);
    printf("\n");

    /*Соберем блок обратно в один, для того, чтобы использовать в последующих тестах*/
    _free(block_2);
    _free(block_1);
}

static void test_3() {
    printf("\nTHIRD_TEST\n");
    void *block_1 = _malloc(10);
    void *block_2 = _malloc(50);
    void *block_3 = _malloc(100);
    debug_heap(stdout, HEAP_START);
    _free(block_2);
    debug_heap(stdout, HEAP_START);
    _free(block_1);
    debug_heap(stdout, HEAP_START);
    printf("\n");

    /*Соберем блок обратно в один, для того, чтобы использовать в последующих тестах*/
    _free(block_3);
    _free(block_1);
}

static void test_4() {
    printf("\nFOURTH_TEST\n");
    debug_heap(stdout, HEAP_START);
    void *addr = _malloc(9000);
    debug_heap(stdout, HEAP_START);
    printf("\n");

    /*Соберем блок обратно в один, для того, чтобы использовать в последующих тестах*/   
    _free(addr);
}

void make_tests() {
    heap_init(24);
    test_1();
    test_2();
    test_3();
    test_4();
}